Version: 0.3.0

Glittery is a static site generator, it aims to be
- Simple by Default, Powerfull When Needed
- Generic, suits all kind of people
- Fast, building site should be fast
- Fully localization support

TODO: write intro

# Site Directory Structure
Every Site folder in Glittery should have the following structure:

``` sh
.                # Site folder
├── pages        # Pages root folder
├── public       # Static pages gose here
├── themes       # Themes folder
├── site.yaml    # Site configuration file
└── .status      # Site info for internal use
```


# Overview
Here is an overview for each directory and file.


## `site.yaml`
This is where Glittery's look for the sites info and configuration.


## `pages`
This folder contains site pages.


## `public`
This is where Glittery store the generated site, it represente the `/` folder
for the site.  It also contains other resources that is used by the site pages
such as images and videos.


## `themes`
This folder contains all the themes available to use by the site.


## `.status`
This folder contains the necessary files to keep track of the site, such as log
files, content hashes and many other things that Glittery needs for internal
use.


# `site.yaml` File
This file contains all the informations and configuration that effect the
site. It is written in [Yaml](http://yaml.org/).  Every site must have this
file.

There are two main section we can use in this file, `info` and `config`.

The `info` section is mandatory and meant to contain information about the site,
here are all key/value pairs that's Glittery looks for:

``` yaml
# info section
info:
  # site langauge
  en:
    # Site title
    title: My Pretty Life

    # Optinal Site summary
    summary:
      Here I write about things that I found it interesting.
      I also do some tutorials on Blender, GIMP, Inkcspace.

    # Optinal Site author
    author: Muhannad Alrusayni

    # Optinal Author email
    author email: muhannad.alrusayni@gmail.com

    # Optional Site creation date, Glittery will write the date for you when you
    # create the site
    creation date: 2018-05-27 07:32:00 -8

    # Category that your site belong to.
    # When omitted Glittery will use `[ Uncategorized ]` as default value
    categories:
      - Programming
      - Photography
      - Reading

    # Optional Keywords that you want the search engines match with your site
    keywords:
      - Programming
      - Rust
      - C
      - Emacs
      - Photography
      - GIMP
      - Blender
      - Inkcspace
```

Users can defined thier info key/value pairs as well, e.g. we can defined `my
books` key with list of books:

``` yaml
info:
  en:
    # other info key/value pairs ...(omited)

    # This is a user defined key/value
    my books:
      - Programming Rust
      - Design Patterns Elements of Reuasble Object-Oriented Software
```

The `config` section is optional, sinces all of its keys have default values,
it's meant to contain the configuration for the site, here is all key/value
pairs that can be used inside this section:

``` yaml
# config section
config:
  # Base url for your site, some time you want to change this to '/blog/' so
  # that your site absolute url will looks like that 'localhost/blog/'.
  # The default value is '/'
  base url: /

  # This is the server config and its possible key/values
  server:
    # Port to listen on, you usually want to use '80' on real environment
    # The default value is '8000'
    port: 8000

    # Address to listen on, you usually want to change this value to your server
    # IP or you can use '0.0.0.0' to listen on every possible address
    # The default is '127.0.0.1'
    address: 127.0.0.1
```


# `pages` Folder
This folder contains all pages in the site.  Every site must have it, its
content consist of **Page**s and **Section**s.

This folder is **Section** folder (we will talk about section later).
Since this folder is a **Section**, then it follows all **Section** rules.  And
this is the root section.

Each **Page** and **Section** have unique ID, Sections ID end with `/` while
pages doesn't.  the root Section have the ID `/`.  In Glittery IDs are similar
to paths in file systems.


## Page
A page is a single file.

``` sh
.                        # Site folder
├── pages                # Pages folder, root section (id = "/")
│   ├── Rust Intro.md    # Page file (id = "/Rust Intro")
...
```

This file consist of two main sections, the first section called **Front
Matter** and the second section called **Content**.

Here is a simple example of this file:

``` yaml
template: post.tera
title: My Emacs Configuration
summary: Showing and explaining my emacs configuration
creation date: 2018-05-28 08:32:00 -8
tags:
  - Emacs
  - Programming
  - Editing
  - Configuration
---

# Intro
In this post I will show my Emacs config file and explain each line in it.

# Packages
..etc
```

Glittery will split this file in the first appear of the string `---`, and will
treat the first section as **Front Matter** and the second section as
**Content**. **Note** that `---` string must have new line before and after it.

The **Content** section is optional and can be omited. If the page file didn't
contain the string `---`, it means the page file doesn't have **Content**
section.

If the page file didn't have the **Front Matter** section then Glittery treat
this as an error.


### Front Matter Section
The **Front Matter** section written in [Yaml](http://yaml.org/) format. Here is
all the key/value pairs that Glittery looks for:

``` yaml
# Optinal flag, Whether this page is draft or not.  Glittery will write `true`
# for you when you create page for the first time
# When omitted `false` will be used.
draft: true

# Template ID, used to render this page
template: info/about me.tera

# Page's title
# When omited the last part of page ID will be used.
title: About Me

# Optional Page's Summary
summary:
  A brief about Me and things that I'm interested in.

# Optional Page's author.  Glittery will write the site author value when page
# created for the first time.  When omitted Glittery will use site author value.
author: Muhannad Alrusayni

# Optional Page's author email. Same as page's author
author email: muhannad.alrusayni@gmail.com

# Optional Page creation date.  Glittery will write the date for you when you
# create the page for the first time
creation date: 2018-05-28 08:32:00 -8

# Optional List of categories that your page belong to.  When omitted Glittery
# will use `[ Uncategorized ]` as default value
categories:
  - Personal Info

# Optional Tags your page belong to.  When omitted Glittery will use `[ Untagged
# ]` as default value
tags:
  - About Me
  - Whoami
  - My Info
```

**Front Matter** can also contain key-value pairs that are defined by page
authors.

Note: The key `content` is reserved for the **Content** section.

### Content Section
The **Content** section useful when you want to write a page that have a big
piece of text that cannot be splited. such as in posts or news pages.

This section is written in [CommonMark](http://commonmark.org/) (aka Markdown).

**Note**: Currently Glittery only support CommonMark.  Other format may be added
later.

This section starts after the `---` string that comes after **Front Matter**
section.

The **Content** section have access to some of the Tera functions.  This is
super handy when you want to compute some numbers, do some string formating also
Glittery have some Tera global functions that is designed to be used by
**Content** section. (See `Global Functions` chapter)


## Section
A section is a folder inside the `pages` folder, its purpose is to group pages,
for example we can group posts pages in section called `/posts/`, we can also
have nested sections, such as `/posts/gimp tutorials/`:

``` sh
.                                 # Site folder
├── pages                         # `/` -> Root Section
│   └── posts                     # `/posts` -> Section
│       ├── gimp tutorials        # `/posts/gimp tutorials/` -> Section
│       │   ├── 1 - intro.md      # `/posts/gimp tutorials/1 - intro` -> Page
│       │   └── 2 - tools.md      # `/posts/gimp tutorials/2 - tools` -> Page
│       ├── My Emacs Config.md    # `/posts/My Emacs Config` -> Page
│       └── Rust Intro.md         # `/posts/Rust Intro` -> Page
...
```

Note: The section ID `resources` is reserved for internal use.

### Section Page
A section can also have its own page, we can give a section a page by adding a
new file called `index.md` inside that section.

Let's say we want a page for the section `posts` then we add `index.md` file in
`posts` section, and its directory structure would look like:

``` sh
.                       # Site folder
├── pages               # `/` -> Root section
│   ├── posts           # `/posts/` -> Section
│   │   ├── index.md    # `/posts/index` -> Section Page
│   │   ...
...
```


## Building `pages` Folder
Glittery will build every page in `pages` folder into an HTML page, these HTML
pages will be stored in the `public` folder, the output HTML page will have the
same path they have in `pages` folder, except `.md` extension will be `.html`.

So if we have these pages:

``` sh
.                                 # Site folder
├── pages                         # pages folder
│   └── posts
│       ├── gimp tutorials
│       │   ├── 1 - intro.md
│       │   └── 2 - tools.md
│       ├── index.md
│       ├── My Emacs Config.md
│       └── Rust Intro.md
...
```

Their HTML output will be:

``` sh
.                                   # Site folder
├── public                          # public folder
│   ├── posts
│   │   ├── gimp tutorials
│   │   │   ├── 1 - intro.html
│   │   │   └── 2 - tools.html
│   │   ├── index.html
│   │   ├── My Emacs Config.html
│   │   └── Rust Intro.html
...
```

Building page goes through **five** phases:
1. The **Content** section get processed by Tera preprocessor, the **Front
   Matter** is passed as context, so it's key/value pairs are accessible in this
   phase.
2. The result of first phase get processed by CommonMark preprocessor.
3. save the result from phase 2 in **Content**. So **Content** section will have
   the processed value.
4. The selected template in **Front Matter** get processed by Tera preprocessor,
   using **Front Matter** and the new **Content** as context.
5. Save the result from phase 4 to the HTML page.

All these phases doesn't change the content of the page file, it process it in
memory buffer.


# `public` Folder
This folder contains all the generated HTML pages as well as thier resources.
Resources live in side subfolder called `resources`.  the directory structure
for public folder is:

``` sh
.                    # Site folder
├── public           # Root folder for HTML pages
│   └── resources    # Root folder for site resources
...
```


## The `resources` folder
This folder purpose is to contain all the site resources, resources can be any
file used in site pages, such as images, videos, audios or any thing.

Every resources in this folder have ID, its similar to pages ID story, here is a
list of resources with thier ID:

``` sh
.                            # Site folder
├── public                   # root folder for HTML pages
│   └── resources            # `/` -> root resources section
│       ├── gimp works       # `/gimp works/` -> Section
│       │   ├── tut-1.xcf    # `/gimp works/tut-1.xcf` -> Resources
│       │   └── tut-2.xcf    # `/gimp works/tut-2.xcf` -> Resources
│       ├── logo.svg         # `/logo.svg` -> Resources
│       └── Rust-logo.png    # `/Rust-logo.png` -> Resources
...
```

Note: The section ID `theme` is reserved for the theme resources folder so it
cannot be used.  Page authors can not use resources found in the section
`theme`, these resources is only accessible through the theme templates.

# Site Theme
Themes used by the site live in the folder `themes`, this folder contains all
the themes available to be used by the site.

Each subfolder in this folder consider a theme folder, a theme folder contains
templates and thier resources to layout the site pages. It has the following
directory structure:

``` sh
.                         # Site folder
├── themes                # Themes folder
│   ├── light moon        # Theme folder, id `/light moon`
│   │   ├── resources     # Resources used by `/light moon` theme
│   │   │   └── css       # CSS libraries used by `/light moon` theme
│   │   ├── templates     # Templates used by `/light moon` theme
│   │   │   └── std       # Stander Templates Library used by `/light moon`
│   │   │                 # theme
│   │   └── theme.yaml    # Configuration file used by `/light moon` theme
...
```


## `resources` Folder
This folder stores resources that is used by templates, such as CSS, icons and
images ..etc.  It's an optional folder.

This folder will be copied into `public/resources/theme/`, so pages authors
should not edit contents in that folder, since its managed by Glittery itself.
The `theme` folder name can be changed from the `theme.yaml` configuration file.

Note: It is recommanded not put big files in theme resources, since they will be
copied into `public/resources/theme/`

Theme creator can only use resources in this folder and they have no access to
any other resources.

### `css` Folder
This folder meant to contain css libraries that is used by this theme. Its
optional.


## `templates` Folder
This is the root folder that contains all the templates used by site's pages to
render their contents.  Templates are written using
[Tera](https://tera.netlify.com/).


### `std` Folder
Glittery provide some generic and standard templates in this folder that helps
theme designers to build thier themes easly and quickly.


### Tera Templates
Glittery use [Tera](https://tera.netlify.com/) template engine to render its
pages.  (See [Tera Docs](https://tera.netlify.com/docs/installation/))

Tera engine is used in two places (See Building `pages` Folder).  different
configuration is used in each place.


#### Tera Configuration With Content Section
Glittery pass **Content** section to Tera engine as template with **Front
Matter** section as context.  Doing this give pages authors some powerfull
features that Tera engine provide. such as global functions.

The context in this configuration is the **Front Matter** section and can be
accessed by writing the key name, e.g. `{{ author }}`.

In this configuration Glittery doesn't give Tera engine any glob or templates,
so `include` and `inheritance` is not useful here.


#### Tera Configuration With Template
Glittery pass the selected template by the page to Tera engine as template with
**Front Matter** and **Content** sections as context.

The context in this configuration is both **Front Materr** and **Content**
sections, they both can be accessed by writing the key name, so if we want to
access the **Content** value, we write `{{ content }}`, or if want get the page
title we write `{{ title }}` and so on.

In this configuration Glittery give's Tera engine the template folder
`theme/templates/` as glob path, so the template name for the file located at
`theme/templates/posts.tera` will be `posts.tera`.


#### Global Functions
TODO LIST:
- Add more functions
- Add `url(id)` that return URL for resources or page
- Add `pagination_by_date(id, limit)` that return an array of urls for pages in
  section specified by the `id` argument
- Add `pagination_by_link(id, limit)` that return an array of urls for pages in
  section specified by the `id` argument, based on `previous page` and `next
  page` keys found in **Front Mater**.
- Add `fluent(msgid, args...)` that return message text from the FTL files in
  `local` folder

Glittery ships with some tera global functions that makes writing templates a
easier, Here are the full list for each function:


##### `fluent(msgid, ...)` Function
This function used by template files to get translation from fluent files found
in `local` folder that live in theme folder.

Glittery will go through these steps when `fluent` function used:
- Find the language folder inside `local` folder
- Find the fluent file used by the template
- Find the message used by the template, using the message ID
- Return the message text

###### Arguments
`fluent` function use one or more arguments depend on the context

TODO: complete this docs


## `theme.yaml` File
This file contains all the configuration and information that belong to the
theme.

``` yaml
# Theme authors
authors:
  - Muhannad Alrusayni <muhannad.alrusayni@gmail.com>

description:
  Short description for the theme gose here

# Theme license
license: CC BY-SA

# Theme version
# The default is `0.1`
version: 1.2

# supported language, here `en` is the main langauge becuse it comes first
# The default is `[ en ]`
languages:
  - en
  - ar
  - ja

# Theme sources URL, updates are fetched from here
source: https://gitlab.com/0x3uh4224d/Light-Moon.git

# This is the template ID for the site home page
# The default value is `home.tera`
home template: home.tera

# This is the template ID for the page used when vistor visit non-existing page
# The default value is `not-found.tera`
not found template: not-found.tera

# This is the theme resources name when it gets copied into the
# `public/resources/` folder
# The default is `theme`
resources output name: theme
```

# `.status` Folder
TODO: write more about this folder

This folder contains the necessary files to keep track of the site, such as log
files, content hashes and many other things that Glittery need for internal
use. Users doesn't need to edit this folder or it's content, since it's created
and managed by Glittery itself.


# Page, Template, Resource and Theme ID
Glittery can identify pages, templates and resources by using thier ID. Here is
how Glittery identify each type of its content:
- Page: the page file path without the root `pages` folder path, also without
  file extension.
- Pages Section: the section folder path without the root `pages` folder path.
- Resources File: the resource file path without the root `resources` folder
  path.
- Resources Section: the section folder path without the root `resources` folder
  path.
- Template file: the template file path without the root `templates` folder
  path.

Here is a full example for the above

``` sh
.                                       # Site folder
├── pages                               # id = `/`
│   └── posts                           # id = `/posts/`
│       ├── gimp tutorials              # id = `/posts/gimp tutorials/`
│       │   ├── 1 - intro.md            # id = `/posts/gimp tutorials/1 - intro`
│       │   └── 2 - tools.md            # id = '/posts/gimp tutorials/2 - tools'
│       ├── index.md                    # id = `/posts/index`
│       ├── My Emacs Config.md          # id = `/posts/My Emacs Config`
│       └── Rust Intro.md               # id = `/posts/Rust Intro`
├── public
│   └── resources                       # id = `/`
│       ├── site-logo.svg               # id = `/site-logo.svg`
│       └── my-cv.pdf                   # id = `/my-cv.pd`
├── themes
│   └── light moon
│       ├── resources                   # id = `/`
│       │   └── css                     # id = '/css/'
│       │       ├── fonts               # id = '/css/fonts/'
│       │       │   └── amiri.ttf       # id = '/css/fonts/amiri.ttf'
│       │       ├── style.css           # id = '/style.css'
│       │       ├── tachyons.css        # id = '/tachyons.css'
│       │       └── tachyons.min.css    # id = '/tachyons.min.css'
│       ├── templates
│       │   ├── home.tera               # id = 'home.tera'
│       │   ├── not found.tera          # id = 'not found.tera'
│       │   ├── posts.tera              # id = 'posts.tera'
│       │   └── std
│       │       ├── base.tera           # id = 'std/base.tera'
│       │       └── macros.tera         # id = 'std/macros.tera'
...
```

As you can see there is no sections for templates, so there is no IDs for
folders in the `templates` folder.

IDs can be written without the prefix `/`, so the ID `/posts/Rust Intro` can be
written like `posts/Rust Intro`.


# Multilingual Support
The following sections will show you what you need to add multilingual support
for a site and its content.


## Site
Users who want thier site to support more languages will need to add info
for each language in `site.yaml`.

For example, if we want our site to support Enlgish and Arabic languages, we
will write two subsections in `info` section. `site.yaml` would look like this:

``` yaml
info:
  # English info
  en:
    title: Muhnnad Blog
    author: Muhannad Alrusayni
    author email: muhannad.alrusayni@gmail.com
    creation date: 2018-05-27 07:32:00 -8
    categories:
      - Programming
      - Photography
      - Reading

  # Arabic info
  ar:
    title: مدونة مهند
    author: مهند الرسيني
    author email: muhannad.alrusayni@gmail.com
    creation date: ١٤٣٩-٩-١٢ ٧:٣٢:٠٠
    categories:
      - برمجة
      - تصوير
      - قراءة

config:
  server:
    port: 80
```

English info now can be found in `info.en` subsection, and Arabic info in
`info.ar` subsection. each language info can have different values.

The default language can be change to any other langauge e.g. Arabic. That's
done by writing `info.ar` before other languages. `site.yaml` would look like
this:

``` yaml
info:
  # the default languages most be the first languages in `info` section
  ar:
    title: مدونة مهند
    author: مهند الرسيني
    author email: muhannad.alrusayni@gmail.com
    creation date: ١٤٣٩-٩-١٢ ٧:٣٢:٠٠
    categories:
      - برمجة
      - تصوير
      - قراءة

  en:
    title: Muhnnad Blog
    author: Muhannad Alrusayni
    author email: muhannad.alrusayni@gmail.com
    creation date: 2018-05-27 07:32:00 -8
    categories:
      - Programming
      - Photography
      - Reading

config:
  server:
    port: 80
```

When we add info languages subsections we are telling Glittery that all the site
content will support these languages, even the theme should support them.

When you add languages that are not supported by the theme, Glittery will print
warnings when you build the site. Becuse theme strings will be untranslated for
these unsupported languages by the theme.


## Page
When a site is multilingual all of its pages should have their own folders.
Page folder will be the ID and it will contain a page file for each language, so
if we have page `/My Emacs Config` in multilingual site that support English and
Arabic. the page folder will look like this:

``` sh
.
├── pages
│   ├── My Emacs Config
│   │   ├── ar.md    # Arabic version
│   │   └── en.md    # English version
...
```

This way we have two version of `/My Emacs Config` page. The English version
would look like:

``` yaml
template: post.tera
title: My Emacs Configuration
summary: Showing and explaining my emacs configuration
creation date: 2018-05-28 08:32:00 -8
tags:
  - Emacs
  - Programming
  - Editing
  - Configuration
---

In this post we will talk about Rust...
```

And the Arabic version would look like:

``` yaml
template: post.tera
title: إعداداتي لبرنامج إماكس
summary: شرح إعداداتي لبرنامج إماكس
creation date: ١٤٣٩-٩-١٢ ٧:٣٢:٠٠
tags:
  - إماكس
  - برمجة
  - تحرير
  - إعدادات
---

سنتحدث في هذا المقال عن لغة Rust...
```

Glittery will give error when the main langauge file is missed, and will give
warning when other langauge file is missed.


## Theme
Creating multilingual theme is done by adding the languages you want to support
in `languages` key, found in `theme.yaml`.  Each language should have subfolder
in `local` folder.  Here is an example of theme that use English as default
language and Arabic:

``` sh
.
├── themes
│   └── light moon
│       ├── locale
│       │   ├── ar
│       │   │   ├── home.ftl
│       │   │   ├── not found.ftl
│       │   │   ├── posts.ftl
│       │   │   └── std
│       │   │       ├── base.ftl
│       │   │       └── macros.ftl
│       │   └── en
│       │       ├── home.ftl
│       │       ├── not found.ftl
│       │       ├── posts.ftl
│       │       └── std
│       │           ├── base.ftl
│       │           └── macros.ftl
│       ├── templates
│       │   ├── home.tera
│       │   ├── not found.tera
│       │   ├── posts.tera
│       │   └── std
│       │       ├── base.tera
│       │       └── macros.tera
...
```


### `.ftl` Files
These files are [Fluent](https://projectfluent.org/) files that Glittery uses to
give theme creator the ability to support multilingual.  Fluent is powerfull
localization system, See [Fluent Docs](https://projectfluent.org/fluent/guide/).

The way these files is used is by using the global function `fluent` from the
template files.

lets say we have template `home.tera` that contains:

```
{{ fluent(msgid = "greatting") }}
```

When `fluent` get called it will go to the `local/en/home.ftl` file and extract
the message text and return it as function result.


### Generating `.ftl` files
Theme designers shoudn't create `.ftl` files by hand since it's tedious thing to
do, Glittery can generate these files by extracting messages IDs from template
files.

Glittery will generate `local/{LANG}` folder with `.ftl` files for each language
found in `languages` key in `theme.yaml`.  This will help translator to quickly
start translating theme templates.


### Multilingual Template
A template that paln to support other languages must use the global function
`fluent`.  (see `fluent` in `global functions` section)


# URLs Managment
Since we have static site, URLs are the file name in the `public` folder,
`public` folder is the root folder and have url `/`.

URLs in Glittery are all relative, So if we would want to point to the site home
page we would write `/`.  By doing this we make our site portable, it can live
anywhere not only in `public` folder.

Here is an example of `public` folder with URLs shown:

``` sh
.                                 # Site folder
├── public                        # URL = `/`
│   ├── posts                     # URL = `/posts/`
│   │   ├── gimp tutorials        # URL = `/posts/gimp tutorials/`
│   │   │   ├── 1 - intro.html    # URL = `/posts/gimp tutorials/1 - intro.html`
│   │   │   └── 2 - tools.html    # URL = `/posts/gimp tutorials/2 - tools.html`
│   │   ├── index.html            # URL = `/posts/index.html`
│   │   ├── My Emacs Config.html  # URL = `/posts/My Emacs Config.html`
│   │   └── Rust Intro.html       # URL = `/posts/Rust Intro.html`
...
```

Note that `gimp tutorials` doesn't have section page, so a proper page won't be
shown.

Currently Glittery doesn't support changing the URL from the **Front Matter**
This may be add later.


# Glittery Command Line Interface
Glittery have a powerfull and user-friendly Command-Line interface, it does all
what you need to create and build a site, the following are all commands you can
use with `Glittery` and a thier arguments with short description for thier
purpose.

``` sh
$ Glittery help
Glittery 0.3.0
Muhannad Alrusayni <Muhannad.Alrusayni@gmail.com>
Glittery is static website generator, it aims to be super easy and powerfull website generator

USAGE:
    infrastructure [FLAGS] [SUBCOMMAND]

FLAGS:
    -v, --verbose    Be verbose when showing information
    -s, --strict     Be strict and exit on warning
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    new       Create a new website, page, template or theme
    remove    Remove a page, template or theme folder with all its related files
    build     Build the site pages into `public` folder
    check     Check the current website and report errors without building the website
    clean     Remove .status directory in current website
    info      Prints information about the current website
    help      Prints this message or the help of the given subcommand(s)
```

Glittery also ships with built-in documentation for each command, you just need
to write the subcommand followed by `--help`, e.g. `Glittery new --help` and
Glittery will show help message for `new` subcommand

Glittery command line ship with fully documentation, you just need to use the
flag `-h` when you want to show a command docs.
