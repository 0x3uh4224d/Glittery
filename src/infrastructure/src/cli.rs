// TODO: pass documentation to long_help() for each arguments so we have full
//       documentation built-in the command line interface.  Note this has been
//       documented in `docs/manual.md` file

use clap::{App, AppSettings, Arg, SubCommand};

fn new_cmd<'a, 'b>() -> App<'a, 'b> {
    let website = SubCommand::with_name("website")
        .about("Create a new website folder")
        .args(&[
            Arg::with_name("name")
                .index(1)
                .help("Site name, it will be used for site folder")
                .value_name("SITE_NAME")
                .takes_value(true)
                .required(true),
            Arg::with_name("summary")
                .long("summary")
                .help("Site summary")
                .value_name("SUMMARY")
                .takes_value(true),
            Arg::with_name("author")
                .long("author")
                .help("Site author")
                .value_name("AUTHOR_NAME")
                .takes_value(true),
            Arg::with_name("author-email")
                .long("author-email")
                .help("Site author email")
                .value_name("AUTHOR_EMAIL")
                .takes_value(true),
            Arg::with_name("categories")
                .long("categories")
                .help("Categories this site belong to")
                .value_name("CATEGORIES")
                .takes_value(true),
            Arg::with_name("keywords")
                .long("keywords")
                .help("keywords used by this stie")
                .value_name("KEYWORDS")
                .takes_value(true),
        ]);

    let page = SubCommand::with_name("page")
        .about("Create a new page")
        .args(&[
            Arg::with_name("id")
                .index(1)
                .help("Page identifier used for the new page")
                .value_name("PAGE_ID")
                .takes_value(true)
                .required(true),
            Arg::with_name("template")
                .long("template-id")
                .short("t")
                .help("Template identifier used by the new page")
                .value_name("TEMPLATE_ID")
                .takes_value(true),
            Arg::with_name("summary")
                .long("summary")
                .help("A summary for the page")
                .value_name("SUMMARY")
                .takes_value(true),
            Arg::with_name("categories")
                .long("categories")
                .help("List of categories this page belong to")
                .value_name("CATEGORIES")
                .takes_value(true),
            Arg::with_name("tags")
                .long("tags")
                .help("List of tags this page belong to")
                .value_name("TAGS")
                .takes_value(true),
        ]);

    let template = SubCommand::with_name("template")
        .about("Create a new template")
        .args(&[Arg::with_name("id")
            .index(1)
            .help("Template identifier used for the new template")
            .value_name("TEMPLATE_ID")
            .takes_value(true)
            .required(true)]);

    let theme = SubCommand::with_name("theme")
        .about("Create a new theme")
        .args(&[
            Arg::with_name("id")
                .index(1)
                .help("Theme identifier used for the new theme")
                .value_name("THEME_ID")
                .takes_value(true)
                .required(true),
            Arg::with_name("license")
                .long("license")
                .short("l")
                .help("Theme license (GPL3, CC, ..etc)")
                .value_name("LICENSE")
                .takes_value(true),
            Arg::with_name("languages")
                .long("languages")
                .short("L")
                .help("Languages supported by the theme")
                .value_names(&["LANG1", "LANG2"])
                .takes_value(true),
        ]);

    SubCommand::with_name("new")
        .about("Create a new website, page, template or theme")
        .subcommands(vec![website, page, template, theme])
}

fn remove_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("remove")
        .about("Remove a page, template or theme folder with all its related files")
}

fn build_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("build").about("Build the site pages into `public` folder")
}

fn check_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("check")
        .about("Check the current website and report errors without building the website")
}

fn clean_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("clean").about("Remove .status directory in current website")
}

fn info_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("info").about("Prints information about the current website")
}

pub fn interface<'a, 'b>() -> App<'a, 'b> {
    App::new("Glittery")
        .about(
            "Glittery is static website generator, it aims to be super easy \
             and powerfull website generator",
        )
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .global_settings(&[
            AppSettings::DeriveDisplayOrder,
            AppSettings::VersionlessSubcommands,
            AppSettings::ArgRequiredElseHelp,
        ])
        .args(&[
            Arg::with_name("verbose")
                .long("verbose")
                .short("v")
                .multiple(true)
                .takes_value(false)
                .global(true)
                .help("Be verbose when showing information"),
            Arg::with_name("strict")
                .long("strict")
                .short("s")
                .takes_value(false)
                .global(true)
                .help("Be strict and exit on warning"),
        ])
        .subcommands(vec![
            new_cmd(),
            remove_cmd(),
            build_cmd(),
            check_cmd(),
            clean_cmd(),
            info_cmd(),
        ])
}
